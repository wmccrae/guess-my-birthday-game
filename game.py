from random import randint #importing the function randint

username = input("Hello! What is your name?  ")

# Make no more than five guesses of the user's birthday via the random number generator.
for guess_number in range (5):
    month_guess = randint(1, 12) #Guess the month
    year_guess = randint(1924,2004) #Guess the year
    print("Guess #",guess_number + 1,": ",username," were you born in ",month_guess,"/",year_guess,"(y/n)?")
    user_answer = input()
    if user_answer == "y" :
        print("I knew it!")
        break
    elif guess_number == 4 :
        print("I have other things to do. Good bye.")
        break
    else :
        print("Drat! Let me try again!")
